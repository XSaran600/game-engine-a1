﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

public class CommandDesignPluginWrapper : MonoBehaviour {

    const string DLL_NAME = "CommandDesignPlugin.dll";

    [DllImport(DLL_NAME)]
    private static extern void executeCmd();

    [DllImport(DLL_NAME)]
    private static extern void undo();

    [DllImport(DLL_NAME)]
    private static extern void redo();

    [DllImport(DLL_NAME)]
    private static extern float up();

    [DllImport(DLL_NAME)]
    private static extern float down();

    [DllImport(DLL_NAME)]
    private static extern float left();

    [DllImport(DLL_NAME)]
    private static extern float right();

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            float y = up();
            Debug.Log(y);
            transform.position.Set(transform.position.x, transform.position.y + y, transform.position.z);
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            float y = down();
            Debug.Log(y);
            transform.position.Set(transform.position.x, transform.position.y + y, transform.position.z);
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            float x = left();
            Debug.Log(x);
            transform.position.Set(transform.position.x + x, transform.position.y, transform.position.z);
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            float x = right();
            Debug.Log(x);
            transform.position.Set(transform.position.x + x, transform.position.y, transform.position.z);
        }
        if (Input.GetKeyDown(KeyCode.Z))
        {
            undo();
        }
        if (Input.GetKeyDown(KeyCode.X))
        {
            redo();
        }
    }
}
