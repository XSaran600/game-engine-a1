﻿using System.Collections;
using UnityEngine;

// Code Reference from https://www.habrador.com/tutorials/programming-patterns/3-observer-pattern/
// Code written by Jarvis Ortega

namespace ObserverPattern
{
    // Will Observe the objects
    public abstract class Observer
    {
        public abstract void OnNotify();
    }

    public class Key : Observer
    {
        // The gameobject key
        GameObject keyObjects;
        // What the key will do when the event happens
        KeyEvents keyEvent;

        public Key(GameObject keyObjects, KeyEvents keyEvent)
        {
            this.keyObjects = keyObjects;
            this.keyEvent = keyEvent;
        }

        // THe key will jump when it is notified
        public override void OnNotify()
        {
            Jump(keyEvent.GetJumpForce());
        }

        void Jump(float jumpForce)
        {
            //If the box is close to the ground
            if (keyObjects.transform.position.y < 0.55f)
            {
                keyObjects.GetComponent<Rigidbody>().AddForce(Vector3.up * jumpForce);
            }
        }
    }
}