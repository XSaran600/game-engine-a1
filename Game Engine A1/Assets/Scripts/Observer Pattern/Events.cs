﻿using System.Collections;
using UnityEngine;

// Code Reference from https://www.habrador.com/tutorials/programming-patterns/3-observer-pattern/
// Code written by Jarvis Ortega

namespace ObserverPattern
{
    // Cube Event when Capsule gets close
    public abstract class KeyEvents
    {
        public abstract float GetJumpForce();
    }

    public class JumpUp : KeyEvents
    {
        public override float GetJumpForce()
        {
            return 30f;
        }
    }
}