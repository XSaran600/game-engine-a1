﻿using System.Collections;
using UnityEngine;

// Code Reference from https://www.habrador.com/tutorials/programming-patterns/3-observer-pattern/
// Code written by Jarvis Ortega

namespace ObserverPattern
{
    public class GameController : MonoBehaviour
    {
        public GameObject robotObject;

        // The key that will observe the robot
        public GameObject keyObject;

        // This will send notifications whenever an event happens
        Subject subject = new Subject();

        // Initialization
        void Start()
        {
            Key key = new Key(keyObject, new JumpUp());

            // Making the key the observer
            subject.AddObserver(key);
        }

        void Update()
        {
            // Cube should jump when Capsule is at centre of map
            if ((robotObject.transform.position).magnitude < 0.5f)
            {
                subject.Notify();
            }
        }
    }
}