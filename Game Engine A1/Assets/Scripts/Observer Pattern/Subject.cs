﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Code Reference from https://www.habrador.com/tutorials/programming-patterns/3-observer-pattern/
// Code written by Jarvis Ortega

namespace ObserverPattern
{
    public class Subject
    {
        // List for the observers
        List<Observer> observers = new List<Observer>();

        public void Notify()
        {
            for (int i = 0; i < observers.Count; i++)
            {
                // Notify the observers
                observers[i].OnNotify();
            }
        }

        public void AddObserver(Observer observer)
        {
            observers.Add(observer);
        }

        public void RemoveObserver(Observer observer)
        {

        }
    }
}