﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Author: Gabrielle Hollaender

public class Tree : MonoBehaviour
{
    public ForestBuilder myForestBuilder;

    public void changeTreeMesh(Mesh newMesh)
    {
        GetComponent<MeshFilter>().mesh = newMesh;
    }

    public void changeTreeTexture(Material newTexture)
    {
        GetComponent<MeshRenderer>().material = newTexture;
    }

    void Start()
    {
    }

    void Update()
    {

    }
}
