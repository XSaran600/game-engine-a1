﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Author: Gabrielle Hollaender

namespace FlyweightDesignPattern
{
    public class ForestOfTrees : MonoBehaviour
    {
        //public Mesh treeMesh;
        //public Texture treeTexture;
        public ForestBuilder myForestBuilder;
        void Start()
        {
            //starts when the scene is started
            //treeMesh = Resources.Load()
            //treeTexture = Resources.Load();

            myForestBuilder.setTreePosition();
        }

        void Update()
        {

        }
    }
}
   


