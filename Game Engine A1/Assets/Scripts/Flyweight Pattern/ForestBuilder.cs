﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Author: Gabrielle Hollaender

public class ForestBuilder : MonoBehaviour {

    List<Tree> allTrees = new List<Tree>();

    public GameObject treeGameObject;
	// Use this for initialization
	void Start ()
    {
        setTreePosition();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

   /*public void setForestScale()
   {
        for(int i =0; i < allTrees.Count; i++)
        {   
            //how to access all of the trees at once
            allTrees[i].transform.localScale = new Vector3();
        }
   }
   */
      public void setTreePosition()
    {
        for (int i = 0; i < 50; i++)
        {
            GameObject treePrefab;
            treePrefab = Instantiate(treeGameObject) as GameObject;
            addTree(treePrefab.GetComponent<Tree>());
            treePrefab.transform.position = new Vector3(Random.Range(-i * 2.0F, i * 2.0F), 0, Random.Range(-i * 2.0F, i * 2.0F));
            //if you want to randomize the placement of the trees -> rand.range(-i * 2.0F, i*2.0F)
        }
    }
    public void addTree(Tree addOnTrees)
       {
            allTrees.Add(addOnTrees);
       }
}
