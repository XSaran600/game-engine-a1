﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Code referenced from: https://www.habrador.com/tutorials/programming-patterns/1-command-pattern/
// Author: Saran Krishnarja
namespace CommandPattern
{
    public class InputHandler : MonoBehaviour
    {
        // Transform to control character
        public Transform trans;
        // Keys
        private Command button_W, button_A, button_S, button_D, button_Z, button_X;
        
        // Stores commands to undo and redo
        public static List<Command> oldCommands = new List<Command>();
        // The characters start postion
        private Vector3 startPos;
        // Reset the coroutine
        private Coroutine replayCoroutine;
        public static bool shouldStartReplay;
        private bool isReplaying;


        void Start()
        {
            button_W = new MoveUp();
            button_A = new MoveLeft();
            button_S = new MoveBack();
            button_D = new MoveRight();
            button_Z = new UndoCommand();
            button_X = new ReplayCommand();

            startPos = trans.position;
        }

        void Update()
        {
            // If the replay is happening we can't move
            if (!isReplaying)
            {
                HandleInput();
            }

            StartReplay();
        }

        // Key Press check
        public void HandleInput()
        {
            if (Input.GetKeyDown(KeyCode.W))        // Up
            {
                button_W.Execute(trans, button_W);
            }
            else if (Input.GetKeyDown(KeyCode.A))   // Left
            {
                button_A.Execute(trans, button_A);
            }
            else if (Input.GetKeyDown(KeyCode.S))   //Down
            {
                button_S.Execute(trans, button_S);
            }
            else if (Input.GetKeyDown(KeyCode.D))   // Right
            {
                button_D.Execute(trans, button_D);
            }
            else if (Input.GetKeyDown(KeyCode.Z))   // Undo
            {
                button_Z.Execute(trans, button_Z);
            }
            else if (Input.GetKeyDown(KeyCode.X))   // Redo
            {
                button_X.Execute(trans, button_X);
            }
        }

        void StartReplay()
        {
            if (shouldStartReplay && oldCommands.Count > 0)
            {
                shouldStartReplay = false;

                if (replayCoroutine != null)
                {
                    StopCoroutine(replayCoroutine); // Stops the Coroutine so it replays from the beginning
                }

                replayCoroutine = StartCoroutine(ReplayCommands(trans));    // Start Replay
            }
        }

        // Replay Coroutine
        IEnumerator ReplayCommands(Transform trans)
        {
            isReplaying = true;

            trans.position = startPos;

            for (int i=0; i <oldCommands.Count; i++)
            {
                oldCommands[i].Move(trans);

                yield return new WaitForSeconds(0.03f); // Wait before you resume
            }

            isReplaying = false;
        }
    }
}
