﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Code referenced from: https://www.habrador.com/tutorials/programming-patterns/1-command-pattern/
// Author: Saran Krishnarja
namespace CommandPattern
{
    public class ReplayCommand : Command
    {
        public override void Execute(Transform trans, Command command)
        {
            InputHandler.shouldStartReplay = true;  // Start the replay
        }
    }
}