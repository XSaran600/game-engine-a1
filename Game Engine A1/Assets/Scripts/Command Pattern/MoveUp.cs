﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Code referenced from: https://www.habrador.com/tutorials/programming-patterns/1-command-pattern/
// Author: Saran Krishnarja
namespace CommandPattern
{
    public class MoveUp : Command
    {
        public override void Execute(Transform trans, Command command)
        {
            Move(trans);

            InputHandler.oldCommands.Add(command);
        }

        public override void Undo(Transform trans)
        {
            trans.Translate(-trans.forward * moveDis);
        }

        public override void Move(Transform trans)
        {
            trans.Translate(trans.forward * moveDis);
        }
    }
}
