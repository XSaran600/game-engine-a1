﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Code referenced from: https://www.habrador.com/tutorials/programming-patterns/1-command-pattern/
// Author: Saran Krishnarja

namespace CommandPattern
{
    public abstract class Command
    {
        // How much the character moves
        protected float moveDis = 1.0f;
        public abstract void Execute(Transform trans, Command command);
        public virtual void Undo(Transform trans) { }
        public virtual void Move(Transform trans) { }
    }


}
