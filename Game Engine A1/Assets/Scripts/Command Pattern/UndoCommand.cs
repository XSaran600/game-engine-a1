﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Code referenced from: https://www.habrador.com/tutorials/programming-patterns/1-command-pattern/
// Author: Saran Krishnarja
namespace CommandPattern
{
    public class UndoCommand : Command
    {
        public override void Execute(Transform trans, Command command)
        {
            List<Command> oldCommands = InputHandler.oldCommands;

            if (oldCommands.Count > 0)
            {
                // Get the previous command
                Command priorCommand = oldCommands[oldCommands.Count - 1];

                // Move the character
                priorCommand.Undo(trans);

                // Remove the command from the list
                oldCommands.RemoveAt(oldCommands.Count - 1);
            }
        }
    }
}
